<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Add Client') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg p-5">

                <form method="POST" action="/client">

                    <div class="form-group">
                        <label>First name*</label>
                        <input name="first_name" class="bg-gray-100 border border-gray-400 leading-normal resize-none w-full h-20 py-2 px-3  placeholder-gray-700 focus:outline-none focus:bg-white"  placeholder='Enter your clients first name' />
                        @if ($errors->has('first_name'))
                            <span class="text-danger">{{ $errors->first('first_name') }}</span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label>Last name*</label>
                        <input name="last_name" class="bg-gray-100 border border-gray-400 leading-normal resize-none w-full h-20 py-2 px-3  placeholder-gray-700 focus:outline-none focus:bg-white"  placeholder='Enter your clients last name' />
                        @if ($errors->has('last_name'))
                            <span class="text-danger">{{ $errors->first('last_name') }}</span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label>Date of birth*</label>
                        <input name="dateofbirth" class="bg-gray-100 border border-gray-400 leading-normal resize-none w-full h-20 py-2 px-3  placeholder-gray-700 focus:outline-none focus:bg-white"  placeholder='Enter your clients date of birth' />
                        @if ($errors->has('dateofbirth'))
                            <span class="text-danger">{{ $errors->first('dateofbirth') }}</span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label>Length (cm)*</label>
                        <input name="length" type="number" class="bg-gray-100 border border-gray-400 leading-normal resize-none w-full h-20 py-2 px-3  placeholder-gray-700 focus:outline-none focus:bg-white"  placeholder='Enter your clients length' />
                        @if ($errors->has('length'))
                            <span class="text-danger">{{ $errors->first('length') }}</span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label>Email address*</label>
                        <input name="email" type="email" class="bg-gray-100 border border-gray-400 leading-normal resize-none w-full h-20 py-2 px-3  placeholder-gray-700 focus:outline-none focus:bg-white"  placeholder='Enter your clients length' />
                        @if ($errors->has('length'))
                            <span class="text-danger">{{ $errors->first('length') }}</span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label>Photo (200x200)</label>
                        <input name="photo" type="file" class="bg-gray-100 border border-gray-400 leading-normal resize-none w-full h-20 py-2 px-3  placeholder-gray-700 focus:outline-none focus:bg-white"  placeholder='Enter your clients length' />
                        @if ($errors->has('photo'))
                            <span class="text-danger">{{ $errors->first('photo') }}</span>
                        @endif
                    </div>

                    <div class="form-group mt-3">
                        <button type="submit" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Add Client</button>
                    </div>
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>
</x-app-layout>
