<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Add Client') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg p-5">

                <form method="POST" action="/client">

                    <div class="form-group">
                        <label>First name*</label>
                        <input name="first_name" class="bg-gray-100 border border-gray-400 leading-normal resize-none w-full h-20 py-2 px-3  placeholder-gray-700 focus:outline-none focus:bg-white" value="{{$client->first_name}}"  placeholder='Enter your clients first name' />
                        @if ($errors->has('first_name'))
                            <span class="text-danger">{{ $errors->first('first_name') }}</span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label>Last name*</label>
                        <input name="last_name" class="bg-gray-100 border border-gray-400 leading-normal resize-none w-full h-20 py-2 px-3  placeholder-gray-700 focus:outline-none focus:bg-white"  value="{{$client->last_name}}"  placeholder='Enter your clients last name' />
                        @if ($errors->has('last_name'))
                            <span class="text-danger">{{ $errors->first('first_name') }}</span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label>Date of birth*</label>
                        <input name="dateofbirth" class="bg-gray-100 border border-gray-400 leading-normal resize-none w-full h-20 py-2 px-3  placeholder-gray-700 focus:outline-none focus:bg-white"  value="{{$client->dateofbirth}}"  placeholder='Enter your clients date of birth' />
                        @if ($errors->has('dateofbirth'))
                            <span class="text-danger">{{ $errors->first('dateofbirth') }}</span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label>Length (cm)*</label>
                        <input name="length" type="number" class="bg-gray-100 border border-gray-400 leading-normal resize-none w-full h-20 py-2 px-3  placeholder-gray-700 focus:outline-none focus:bg-white"  value="{{$client->length}}" placeholder='Enter your clients length' />
                        @if ($errors->has('length'))
                            <span class="text-danger">{{ $errors->first('length') }}</span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label>Email address*</label>
                        <input name="email" type="email" class="bg-gray-100 border border-gray-400 leading-normal resize-none w-full h-20 py-2 px-3  placeholder-gray-700 focus:outline-none focus:bg-white" value="{{$client->email}}"  placeholder='Enter your clients length' />
                        @if ($errors->has('email'))
                            <span class="text-danger">{{ $errors->first('email') }}</span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label>Photo (200x200)</label>
                        <input name="photo" type="file" class="bg-gray-100 border border-gray-400 leading-normal resize-none w-full h-20 py-2 px-3  placeholder-gray-700 focus:outline-none focus:bg-white"  placeholder='Enter your clients photo' />
                        @if ($errors->has('photo'))
                            <span class="text-danger">{{ $errors->first('photo') }}</span>
                        @endif
                    </div>

                    <div class="form-group mt-3">
                        <button type="submit" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Save Client</button>
                    </div>
                    {{ csrf_field() }}
                </form>

                <h2 class="text-2xl">Measurements</h2>

                <div>
                    <h3>Add new measurement to client</h3>
                    <form method="post" action="/client/{{$client->id}}/measurement">

                        <div class="form-group">
                            <label>Weight (kg)*</label>
                            <input name="weight" class="bg-gray-100 border border-gray-400 leading-normal resize-none w-full h-20 py-2 px-3  placeholder-gray-700 focus:outline-none focus:bg-white"  placeholder='Enter weight' />
                            @if ($errors->has('weight'))
                                <span class="text-danger">{{ $errors->first('weight') }}</span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Fat percentage</label>
                            <input name="fat_percentage" class="bg-gray-100 border border-gray-400 leading-normal resize-none w-full h-20 py-2 px-3  placeholder-gray-700 focus:outline-none focus:bg-white"  placeholder='Enter fat percentage' />
                        </div>

                        <div class="form-group">
                            <label>Blood pressure</label>
                            <input name="blood_pressure" class="bg-gray-100 border border-gray-400 leading-normal resize-none w-full h-20 py-2 px-3  placeholder-gray-700 focus:outline-none focus:bg-white"  placeholder='Enter blood pressure' />
                        </div>

                        <button type="submit" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Add Measurement</button>
                        {{ csrf_field() }}

                    </form>
                </div>


                <table class="w-full text-md rounded mb-4">
                    <thead>
                    <tr class="border-b">
                        <th class="text-left p-3 px-5">Weight (kg)</th>
                        <th class="text-left p-3 px-5">Fat percentage</th>
                        <th class="text-left p-3 px-5">Blood pressure</th>
                        <th class="text-left p-3 px-5">Actions</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($client->measurements() as $measurement)
                        <tr class="border-b hover:bg-orange-100">
                            <td class="p-3 px-5">
                                {{$measurement->weight}}
                            </td>
                            <td class="p-3 px-5">
                                {{$measurement->fat_percentage}}
                            </td>
                            <td class="p-3 px-5">
                                {{$measurement->last_name}}
                            </td>
                            <td class="p-3 px-5">

                                <a href="/client/{{$client->id}}" name="edit" class="mr-3 text-sm bg-blue-500 hover:bg-blue-700 text-white py-1 px-2 rounded focus:outline-none focus:shadow-outline">Edit</a>
                                <form action="/client/{{$client->id}}" class="inline-block">
                                    <button type="submit" name="delete" formmethod="POST" class="btn btn-gray text-sm bg-gray-500 hover:bg-gray-700 text-white py-1 px-2 rounded focus:outline-none focus:shadow-outline">Delete</button>
                                    {{ csrf_field() }}
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</x-app-layout>
