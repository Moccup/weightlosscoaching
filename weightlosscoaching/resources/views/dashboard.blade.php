<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="container max-w-7xl mx-auto sm:px-6 lg:px-8">

        <div class="table-container">

            <h2 class="">Clients</h2>

            <table class="w-full text-md rounded mb-4">
                <thead>
                <tr class="border-b">
                    <th class="text-left p-3 px-5">First name</th>
                    <th class="text-left p-3 px-5">Last name</th>
                    <th class="text-left p-3 px-5">Actions</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($clients as $client)
                    <tr class="border-b hover:bg-orange-100">
                        <td class="p-3 px-5">
                            {{$client->first_name}}
                        </td>
                        <td class="p-3 px-5">
                            {{$client->last_name}}
                        </td>
                        <td class="p-3 px-5">

                            <a href="/client/{{$client->id}}" name="edit" class="mr-3 text-sm bg-blue-500 hover:bg-blue-700 text-white py-1 px-2 rounded focus:outline-none focus:shadow-outline">Edit</a>
                            <form action="/client/{{$client->id}}" class="inline-block">
                                <button type="submit" name="delete" formmethod="POST" class="btn btn-gray text-sm bg-gray-500 hover:bg-gray-700 text-white py-1 px-2 rounded focus:outline-none focus:shadow-outline">Delete</button>
                                {{ csrf_field() }}
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>

        <a href="/client" class="btn btn-blue bg-blue-500 hover:bg-blue-700 py-2 px-4 mt-4">
            Add client
        </a>

    </div>


</x-app-layout>
