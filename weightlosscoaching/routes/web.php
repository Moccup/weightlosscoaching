<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ClientController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    redirect('login');
});



Route::middleware(['auth:sanctum', 'verified'])->group(function(){
    Route::get('/',[ClientController::class, 'index'])->name('dashboard');
    Route::get('/dashboard',[ClientController::class, 'index'])->name('dashboard');

    Route::get('/client',[ClientController::class, 'add']);
    Route::post('/client',[ClientController::class, 'create']);

    Route::get('/client/{client}', [ClientController::class, 'edit']);
    Route::post('/client/{client}', [ClientController::class, 'update']);
    Route::post('/client/{client}/measurement', [ClientController::class, 'createmeasurement']);
});


