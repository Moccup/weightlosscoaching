<?php
/**
 * Client Measurement Model
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClientMeasurement extends Model
{
    use HasFactory;

    protected $table = 'client_measurement';

    public function client()
    {
        return $this->belongsTo(Client::class);
    }
}
