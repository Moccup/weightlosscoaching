<?php

namespace App\Http\Controllers;

use App\Models\ClientMeasurement;
use Illuminate\Http\Request;

class ClientMeasurementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ClientMeasurement  $clientMeasurement
     * @return \Illuminate\Http\Response
     */
    public function show(ClientMeasurement $clientMeasurement)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ClientMeasurement  $clientMeasurement
     * @return \Illuminate\Http\Response
     */
    public function edit(ClientMeasurement $clientMeasurement)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ClientMeasurement  $clientMeasurement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ClientMeasurement $clientMeasurement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ClientMeasurement  $clientMeasurement
     * @return \Illuminate\Http\Response
     */
    public function destroy(ClientMeasurement $clientMeasurement)
    {
        //
    }
}
