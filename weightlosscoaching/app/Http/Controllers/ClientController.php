<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\ClientMeasurement;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $clients = DB::table('client')->paginate(15);

        return view('dashboard', ['clients' => $clients]);
    }

    /**
     * Display client add form
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function add()
    {
        return view('client_add');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'dateofbirth' => 'required',
            'length' => 'required',
            'email' => 'required',
        ]);

        $client = new Client();
        $client->first_name = $request->first_name;
        $client->last_name = $request->last_name;
        $client->dateofbirth = $request->dateofbirth;
        $client->length = $request->length;
        $client->email = $request->email;

        $client->save();
        return redirect('/dashboard');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        if ($client->id)
        {
            return view('client_edit', compact('client'));
        }
        else {
            return redirect('/dashboard');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        //

        $client->first_name = $request->first_name;
        $client->last_name = $request->last_name;
        $client->dateofbirth = $request->dateofbirth;
        $client->length = $request->length;
        $client->email = $request->email;

        $client->save();
        return redirect('/dashboard');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        //
    }

    public function createmeasurement(Request $request, Client $client) {

        if ($client->id) {


                $this->validate($request, [
                    'weight' => 'required'
                ]);

                // Assign request values to measurement
                $clientMeasurement = array();
                $clientMeasurement['weight'] = $request->weight;
                $clientMeasurement['fat_percentage'] = $request->fat_percentage;
                $clientMeasurement['blood_pressure'] = $request->blood_pressure;
                $clientMeasurement['client_id'] = $client->id;

                $client->measurements()->insert($clientMeasurement);

                // $client->save();
            return view('client_edit', compact('client'));

        } else {
            return view('dashboard');
        }


    }
}
