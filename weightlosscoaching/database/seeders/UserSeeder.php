<?php
/**
 * User seeder class - creates user entries in database
 */
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Coach Spartner',
            'email' => 'coach@spartner.nl',
            'password' => Hash::make('demo'),
        ]);
    }
}
